const mongoose = require('mongoose');

const thingSchema = mongoose.Schema({
    civilite: { type: String },
    lastName: { type: String },
    firstName: { type: String },
    year: { type: Number },

    email: { type: String },
    phone: { type: String },

    // address: { type: String },
    ville: { type: String },
    pays: { type: String },
    // codepostal: { type: String },

    work: { type: String },
    school: { type: String },

    description: { type: String },
});

module.exports = mongoose.model('Info', thingSchema);