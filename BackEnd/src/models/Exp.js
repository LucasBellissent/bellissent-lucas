const mongoose = require('mongoose');

const expSchema = mongoose.Schema({
    type: { type: String, require: true },

    start: { type: Date, require: true },
    end: { type: Date, require: true },

    entreprise: { type: String, require: true },
    poste: { type: String, require: true },

    description: { type: String, require: true },
    env: [{ techno: String }],
})

module.exports = mongoose.model('Exp', expSchema);