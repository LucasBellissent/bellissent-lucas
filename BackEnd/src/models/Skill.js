const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const skillSchema = mongoose.Schema({
    nom: { type: String, require: true, unique: true },
    pourcent: { type: Number, require: true },
    type: { type: String, require: true },
})

skillSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Skill', skillSchema);