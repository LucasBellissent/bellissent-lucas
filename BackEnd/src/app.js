const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const path = require('path')

const userRoutes = require('./routes/user');
const infoRoutes = require('./routes/info');
// const deleteRoutes = require('./routes/delete');
const adminRoutes = require('./routes/admin');
const skillRoutes = require('./routes/skill');
const expRoutes = require('./routes/exp');


const app = express();

mongoose.connect('mongodb+srv://admin:obito555@cluster0.ua27s.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true
    })
    .then(() => console.log('Connexion à MongoDB réussie !'))
    .catch(err => console.error('Failed', err));

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});

app.use(bodyParser.json());

app.use('/images', express.static(path.join(__dirname, 'images')));

app.use('/api/auth', userRoutes);
app.use('/api/info', infoRoutes);
app.use('/api/admin', adminRoutes);
app.use('/api/skill', skillRoutes);
app.use('/api/exp', expRoutes);

module.exports = app;