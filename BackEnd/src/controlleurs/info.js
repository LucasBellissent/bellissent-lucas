const Info = require('../models/Info');
const jwt = require('jsonwebtoken');
const User = require('../models/User');
var nodemailer = require('nodemailer');

exports.getinfo = (req, res, next) => {
    Info.findById('616c30c855115530c0c9e7d1').then(info => {
            res.status(201).json({...info });
        })
        .catch((error) => res.status(401).json({ message: error }))
}

exports.contact = (req, res, next) => {
    const userId = myid(req);

    User.findById(userId).then(user => {
            if (!user) {
                throw 'Utilisateur non trouvé !';
            }

            Info.findById(user.info).then(info => {

                    if (info) {
                        sendemail(res, 'AtelierLolaC@gmail.com', req.body.mail.objet, ("message provenant de l'adresse mail : " + info.email + '.\n\n\n' + req.body.mail.message))
                    }
                })
                .catch((error) => res.status(401).json({ message: error }))

        })
        .catch((error) => res.status(401).json({ message: error }))
}

function sendemail(res, email, subject, text) {
    var transporter = nodemailer.createTransport({ service: 'gmail', auth: { user: "atelierdelola.contact@gmail.com", pass: "admatlc1006" } });
    var mailOptions = {
        from: 'atelierdelola.contact@gmail.com',
        to: email,
        subject: subject,
        text: text
    };
    transporter.sendMail(mailOptions, function(err) {
        if (err) {
            return res.status(500).send({ message: err.message });
        }
        return res.status(200).json('email envoyé')
    });
}

function myid(req) {
    const token = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.verify(token, 'Token_Projet_Lola_C');
    return decodedToken.userId;
}