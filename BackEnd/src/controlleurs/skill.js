const Skill = require('../models/Skill');

exports.addskill = (req, res, next) => {
    const skill = new Skill({
        nom: req.body.nom,
        pourcent: req.body.pourcent,
        type: req.body.type,
    });
    skill.save()
        .then(() => {
            res.status(200).json({ message: "skill add" });
        })
        .catch(() => {
            res.status(400).json({ message: "can't save" })
        });
}

exports.deleteskill = (req, res, next) => {
    Skill.findOneAndDelete(req.params.id)
        .then(() => res.status(201).json({ message: "skill delete" }))
        .catch(() => res.status(400).json({ message: "can't find that skill" }))
}

exports.modifskill = (req, res, next) => {
    Skill.updateOne({ _id: req.params.id }, {...req.body, _id: req.params.id })
        .then(() => res.status(201).json({ message: 'Skill modifiée !' }))
        .catch(error => res.status(400).json({ error }));
}

exports.getskills = (req, res, next) => {
    Skill.find()
        .then(skills => {
            res.status(201).json(skills);
        })
        .catch(() => {
            res.status(400).json({ message: "no one skill fond" })
        });
}

exports.getsname = async(req, res, next) => {
    try {
        const skills = (await Skill.find()).map(x => x.nom)
        res.status(201).json(skills);
    } catch (error) { res.status(400).json({ error }) };
}