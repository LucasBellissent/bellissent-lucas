const Info = require('../models/Info');
const jwt = require('jsonwebtoken');
const User = require('../models/User');
// const Commande = require('../models/Commande');
// const Formation = require("../models/formation");

fs = require('fs')

exports.createInfo = (req, res, next) => {
    const userId = myid(req);


    User.findOne({ _id: userId }).then(user => {


            // if (!user) {
            //     throw 'Utilisateur non trouvé !';
            // }
            // const formObject = JSON.parse(req.body)
            // console.log(req.body);
            // if (req.files && req.files.length == 2 &&
            //     req.files[0].filename != undefined && req.files[1].filename != undefined) {
            //     formObject.photoProfile = `${req.protocol}://${req.get('host')}/images/${req.files[0].filename}`;
            //     formObject.photoCover = `${req.protocol}://${req.get('host')}/images/${req.files[1].filename}`;
            // }
            Info.updateOne({ _id: user.info }, {...req.body, _id: user.info })
                .then(() => res.status(201).json({ message: 'Informations enregistrées !' }))
                .catch(error => res.status(400).json({ error }));
            // console.log(formObject)

        })
        .catch(error => {
            // console.log(error);
            res.status(401).json({ error })
        });
}

exports.modifInfo = (req, res, next) => {
    const userId = myid(req);

    User.findOne({ _id: userId }).then(user => {
            if (!user) {
                throw 'Utilisateur non trouvé !';
            }
            // const formObject = JSON.parse(req.body)
            // delete formObject._id;
            // const newform = {
            //     ...req.body,
            // };
            // if (req.file && req.file.filename != undefined)
            //     newform.photoProfile = `${req.protocol}://${req.get('host')}/pdf/${req.file.filename}`;

            // console.log(req.body.infos)

            Info.updateOne({ _id: user.info }, {...req.body.infos, _id: user.info })
                .then(() => res.status(201).json({ message: 'informations modifié!' }))
                .catch(error => res.status(400).json({ error }));
            //             Desc.findById({ _id: user.info }).then(desc => {
            //                 if (desc != undefined) {
            //                     console.log(desc.autresPhotos);
            //                     fs.unlinkSync(desc.autresPhotos);  }
            // })
        })
        .catch((error) => res.status(401).json({ message: error }))
}

exports.gtinfo = (req, res, next) => {
    i = 0;
    const userId = myid(req);

    User.findById(userId)
        .then(user => {
            Info.findById(user.info)
                .then(info => {
                    return res.status(200).json(info);
                })
        })
        .catch(error => res.status(404).json({ message: 'ces informations ne correspondent a aucun utilisateur' }));
}

exports.getinfo = (req, res, next) => {
    i = 0;

    Info.findOne({ _id: req.params.id })
        .then(info => {
            User.findOne({ email: info.email })
                .then(user => {
                    obj = {
                        info: info,
                        role: user.roles
                    }
                    return res.status(200).json(obj);
                })
        })
        .catch(error => res.status(404).json({ message: 'ces informations ne correspondent a aucun utilisateur' }));
}

function myid(req) {
    const token = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.verify(token, 'Token_Projet_Lola_C');
    return decodedToken.userId;
}