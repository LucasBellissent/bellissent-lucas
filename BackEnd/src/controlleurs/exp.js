const Exp = require('../models/Exp');

exports.addexp = (req, res, next) => {
    const exp = new Exp({
        type: req.body.type,

        start: req.body.start,
        end: req.body.end,

        entreprise: req.body.entreprise,
        poste: req.body.poste,

        description: req.body.description,
        env: req.body.env,
    });
    exp.save()
        .then(() => {
            res.status(200).json({ message: "exp add" });
        })
        .catch(() => {
            res.status(400).json({ message: "can't save" })
        });
}

exports.deletexp = (req, res, next) => {
    Exp.findOneAndDelete(req.params.id)
        .then(() => res.status(201).json({ message: "exp delete" }))
        .catch(() => res.status(400).json({ message: "can't find that skill" }))
}

exports.modifexp = (req, res, next) => {
    Exp.updateOne({ _id: req.params.id }, {...req.body, _id: req.params.id })
        .then(() => res.status(201).json({ message: 'Experience modifiée !' }))
        .catch(error => res.status(400).json({ error }));
}

exports.getsexp = (req, res, next) => {
    Exp.find()
        .then(exps => {
            res.status(201).json(exps);
        })
        .catch(() => {
            res.status(400).json({ message: "no one skill fond" })
        });
}

exports.getexp = (req, res, next) => {
    Exp.findById(req.params.id).then(exp => {
            res.status(201).json(exp);
        })
        .catch(() => res.status(400).json({ message: "can't find that skill" }))
}