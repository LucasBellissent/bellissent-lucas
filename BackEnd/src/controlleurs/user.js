const User = require('../models/User');
const Info = require('../models/Info');

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
var nodemailer = require('nodemailer');

const _ = require('lodash');

exports.signup = (req, res, next) => {
    bcrypt.hash(req.body.password, 10)
        .then(hash => {
            const user = new User({
                email: req.body.email,
                password: hash,
                roles: 'admin'
            });
            // console.log(user);
            user.save()
                .then(() => {
                    const info = new Info({});
                    info.save()
                        .then(() => {
                            user.info = info._id;
                            user.save()
                                .then(() => {
                                    res.status(200).json({ message: "Votre compte à était crée. Vous pouvez mainentant vous connecter." });
                                })
                                .catch(error => res.status(401).json({ message: "impossible d'enregistrer l'id info" }));
                        })
                        .catch(error => res.status(402).json({ message: 'Ces info existe déja' }));
                })
                .catch(error => res.status(403).json({ message: 'Ce compte existe déja' }));
        })
        .catch(error => res.status(500).json({ error }));
}

// exports.signup = (req, res, next) => {
//     bcrypt.hash(req.body.password, 10)
//         .then(hash => {
//             const user = new User({
//                 email: req.body.email,
//                 password: hash,
//                 roles: 'admin'
//             });
//             user.save().then(usr => {
//                     const info = new Info({});
//                     info.save().then(inf => {
//                             User.updateOne({ _id: user._id }, { info: inf._id }).then(() => {
//                                     res.status(200).send({ message: "Vous avez correctement crée votre compte. Vous pouvez mainentant vous connecter." });
//                                     next();
//                                 })
//                                 .catch(error => { throw "les infos utilisateur n'ont pas pue étre modifiées" });
//                         })
//                         .catch(error => { throw "les infos utilisateur n'ont pas pue étre enregistrer" });
//                 })
//                 .catch(error => res.status(400).json({ message: 'Ce compte existe déja' }));
//         })
//         .catch(error => res.status(500).json({ error }));
// };

exports.login = (req, res, next) => {
    User.findOne({ email: req.body.email })
        .then(user => {
            if (!user) {
                return res.status(401).json({ message: 'Utilisateur non trouvé !' });
            }
            bcrypt.compare(req.body.password, user.password)
                .then(valid => {
                    if (!valid) {
                        return res.status(401).json({ message: 'Mot de passe incorrect !' });
                    }
                    // console.log('ci')

                    res.status(200).json({
                        userId: user._id,
                        roles: user.roles,
                        token: jwt.sign({ userId: user._id },
                            'Token_Projet_Lola_C', { expiresIn: '24h' }
                        )
                    });

                })
                .catch(error => res.status(500).json({ error }));
        })
        .catch(error => res.status(500).json({ error }));
}

exports.forgetpassword = (req, res, next) => {
    User.findOne({ email: req.body.email })
        .then(user => {
            if (!user) {
                return res.status(401).json({ message: "Vous n'avez pas crée de compte" });
            }
            var token = jwt.sign({ userId: user._id },
                'Token_Projet_Lola_C', { expiresIn: '1h' });
            const url = req.headers.host + '\/api/auth/autopassword\/' + token;
            var transporter = nodemailer.createTransport({ service: 'gmail', auth: { user: "atelierdelola.contact@gmail.com", pass: "admatlc1006" } });
            var mailOptions = {
                from: 'atelierdelola.contact@gmail.com',
                to: user.email,
                subject: 'Généré un nouveau mots de passe',
                text: 'Bonjour,\n\n' +
                    'Etes vous sur de vouloir changer votre mots de passe ?  \n\n' +
                    'Si oui, en cliquant sur le lien si dessous un nouveau mots de passe automatiquement généré vous sera envoyer sur cette adresse email : \n\nhttps:\/\/' +
                    url + '   .\n\n' + 'Pour rappel un fois connecter il vous sera possible de changer à nouveau ce mots de passe.\n' +
                    '\n\n' + "Merci d'utiliser nos services et bonne journée." +
                    "\nCordialement; \n\nle service Atellier de Lola\n"
            };
            transporter.sendMail(mailOptions, function(err) {
                if (err) {
                    user.delete();
                    return res.status(500).send({ message: err.message });
                }
                res.status(200).send({ message: 'Un email de vérification a était envoyer à ' + user.email + '.' + " Le token s'autodétruira dans une heure" });
            });

        })
        .catch(error => res.status(500).json({ error }));
};

exports.autopassword = (req, res, next) => {
    const decodedToken = jwt.verify(req.params.token, 'Token_Projet_Lola_C');
    const userId = decodedToken.userId;

    if (req.body.userId && req.body.userId !== userId) {
        return res.status(400).send({ message: "Nous n'avons pas trouver votre token. peut étre a t'il expiré?" });
    } else {
        User.findOne({ _id: userId }, function(err, user) {
            if (!user) return res.status(400).send({ message: "Nous n'avons pas trouver d'utilisateur pour ce token." });
            var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            var string_length = 8;
            var randomstring = '';
            for (var i = 0; i < string_length; i++) {
                var rnum = Math.floor(Math.random() * chars.length);
                randomstring += chars.substring(rnum, rnum + 1);
            }
            bcrypt.hash(randomstring, 10)
                .then(hash => {
                    const obj = {
                        password: hash
                    }
                    user = _.extend(user, obj);
                    user.save(function(err) {
                        if (err) {
                            return res.status(500).send({ message: err.message });
                        }
                        var transporter = nodemailer.createTransport({ service: 'gmail', auth: { user: "atelierdelola.contact@gmail.com", pass: "admatlc1006" } });
                        var mailOptions = {
                            from: 'atelierdelola.contact@gmail.com',
                            to: user.email,
                            subject: 'Nouveau mots de passe',
                            text: 'Bonjour,\n\n' +
                                'Voici votre nouveau mots de passe :' + randomstring + '\n' +
                                'Il est conseiller de le changer une fois connecter dans vos information.' +
                                '\n\n' + "Merci d'utiliser nos services et bonne journée." +
                                "\nCordialement; \n\nle service AJFR\n"
                        };
                        transporter.sendMail(mailOptions, function(err) {
                            if (err) {

                                return res.status(500).send({ message: err.message });
                            }
                            res.status(200).send({ message: 'votre mots de passe a était correctement modifier' });
                        });
                    });
                });
        });
    }
};

exports.changepass = (req, res, next) => {
    const token = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.verify(token, 'Token_Projet_Lola_C');
    const userId = decodedToken.userId;
    User.findOne({ _id: userId }).then(user => {
        if (!user) {
            return res.status(401).json({ message: 'Utilisateur non trouvé !' });
        }
        bcrypt.compare(req.body.oldpw, user.password)
            .then(valid => {
                if (!valid) {
                    return res.status(401).json({ message: 'Votre Mot de passe actuelle est incorrect !' });
                }
                bcrypt.hash(req.body.newpw, 10)
                    .then(hash => {
                        const obj = {
                            password: hash
                        }
                        user = _.extend(user, obj);
                        user.save(function(err) {
                            if (err) {
                                return res.status(500).send({ message: err.message });
                            }
                            res.status(200).send({ message: 'votre mots de passe a était correctement modifier' });
                        })
                    });
            })
            .catch(error => res.status(500).json({ error }));
    })
}