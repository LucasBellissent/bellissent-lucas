const jwt = require('jsonwebtoken');
const User = require('../models/User');

module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(' ')[1];
        const decodedToken = jwt.verify(token, 'Token_Projet_Lola_C');
        const userId = decodedToken.userId;
        User.findOne({ _id: userId }).then(user => {
            if (user.roles != 'admin') {
                throw res.status(401).json({
                    error: new Error("this User is't admin")
                });;
            } else {
                // console.log("he is ok");
                next();
            }
        })

    } catch {
        res.status(401).json({
            error: new Error('Invalid request!')
        });
    }
};