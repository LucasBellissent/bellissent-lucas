const express = require('express');
const router = express.Router();

const auth = require('../middleware/auth');
const admin = require('../middleware/admin');

const skillCtrl = require('../controlleurs/skill');

router.post('/add', auth, admin, skillCtrl.addskill);
router.delete('/delete/:id', auth, admin, skillCtrl.deleteskill);
router.put('/modif/:id', auth, admin, skillCtrl.modifskill);
router.get('/gets', skillCtrl.getskills);

router.get('/getsname', skillCtrl.getsname);

module.exports = router;