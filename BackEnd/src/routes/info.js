const express = require('express');
const router = express.Router();
const infoCtrl = require('../controlleurs/info');

router.get('/getinfo', infoCtrl.getinfo);
router.post('/contact', infoCtrl.contact);

module.exports = router;