const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');

const userCtrl = require('../controlleurs/user');

// router.post('/signup', userCtrl.signup);
router.post('/login', userCtrl.login);

router.post('/resetpassword', userCtrl.forgetpassword);
router.get('/autopassword/:token', userCtrl.autopassword);
router.post('/changepw', auth, userCtrl.changepass);

module.exports = router;