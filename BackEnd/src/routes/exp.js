const express = require('express');
const router = express.Router();

const auth = require('../middleware/auth');
const admin = require('../middleware/admin');

const expCtrl = require('../controlleurs/exp');

router.post('/add', auth, admin, expCtrl.addexp);
router.delete('/delete/:id', auth, admin, expCtrl.deletexp);
router.put('/modif/:id', auth, admin, expCtrl.modifexp);
router.get('/gets', expCtrl.getsexp);
router.get('/get/:id', expCtrl.getexp);

module.exports = router;