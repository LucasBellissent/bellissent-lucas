const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const admin = require('../middleware/admin');
const adminCtrl = require('../controlleurs/admin');

router.post('/createinfo', auth, admin, adminCtrl.createInfo);
router.post('/modifinfo', auth, admin, adminCtrl.modifInfo);
router.get('/getinfo', auth, admin, adminCtrl.gtinfo);

module.exports = router;