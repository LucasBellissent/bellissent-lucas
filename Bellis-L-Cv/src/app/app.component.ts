import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from './services/alert.service';
import { AuthenticationService } from './services/authentification.service';
import { User } from './services/models/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Bellis-L-Cv';
  currentUser?: User ;


  constructor(
    protected alertService: AlertService, 
    private router: Router,
    private authenticationService: AuthenticationService,
    // private userService: UserService
    ) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }
  
  logout() {
    this.authenticationService.logout();
  }
}
