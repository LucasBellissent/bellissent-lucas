import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';
import { ExpService } from 'src/app/services/exp.service';
import { SkillService } from 'src/app/services/skill.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-manage-experience',
  templateUrl: './manage-experience.component.html',
  styleUrls: ['./manage-experience.component.css']
})
export class ManageExperienceComponent implements OnInit {

  loading: boolean = false;

  exps: any =  undefined;

  searchText: string = '';
  type: string = 'entreprise';

  constructor(
    protected alertService: AlertService,
    private expservice: ExpService,
  ) { }

  ngOnInit(): void {
    this.gets();
  }

  gets() {
    this.expservice.getsExp().then(
      (exps) => {
        // console.log(skills)
        this.exps = exps;
      },
      (error) => {
        this.loading = false;
      }
    );
  }

  delete(skill: any) {
    console.log(skill)
    this.expservice.deleteExp(skill).then(
      () => {
        this.loading = false;
        location.reload();
      },
      (error) => {
        this.loading = false;
      }
    );
  }
  
  
  Changedtype(val: any) {
    // console.log(val.value);
    this.type = val.value;
  }
  
  Changed(val: any) {
    this.searchText = val.value;
  }

}
