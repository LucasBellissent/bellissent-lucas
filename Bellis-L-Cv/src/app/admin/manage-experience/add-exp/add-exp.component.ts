import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';
import { ExpService } from 'src/app/services/exp.service';
import { SkillService } from 'src/app/services/skill.service';

@Component({
  selector: 'app-add-exp',
  templateUrl: './add-exp.component.html',
  styleUrls: ['./add-exp.component.css']
})
export class AddExpComponent implements OnInit {
  loading: boolean = false;
  skills: any = undefined;

  newenv(): FormGroup {
    return this.fb.group({
      techno: '',
    })
  }

  types = [
    'School Project',
    'Internship',
    'Pro'
  ]

  profileForm = this.fb.group({
    type: ['', [Validators.required]],
    entreprise: ['', [Validators.required, Validators.minLength(1)]],
    poste: ['', [Validators.required, Validators.minLength(1)]],
    desciption: ['', [Validators.required]],
    
    env: new FormArray([
    ]),
  });

  dateForm = this.fb.group({
    start: ['', [Validators.required]],
    end: ['', [Validators.required]],
  });

  range = new FormGroup({
    start: new FormControl(),
    end: new FormControl()
  });

  constructor(
    protected alertService: AlertService,
    private fb: FormBuilder,
    private skillservice: SkillService,
    private expservice: ExpService,

  ) { }

  ngOnInit(): void {
    this.addenv();
    this.getsskills();
  }

  //env
  get env(): FormArray {
    return this.profileForm.get("env") as FormArray
  }
  addenv() {
    this.env.push(this.newenv());
  }
  deleteenv(index: number) {
    this.env.removeAt(index);
  }

  getsskills() {
    this.skillservice.getsName().then(
      (skills) => {
        // console.log(skills)
        this.skills = skills;
      },
      (error) => {
        this.loading = false;
      }
    );
  }

  onSubmit() {
    this.loading = true;
    const exp: {} = {
      type: this.profileForm.get('type')!.value,

      start: this.range.value.start as Date,
      end: this.range.value.end as Date,

      entreprise: this.profileForm.get('entreprise')!.value,
      poste: this.profileForm.get('poste')!.value,
      description: this.profileForm.get('desciption')!.value,
      env: this.profileForm.get('env')!.value,
    };
    // console.log(exp)
    this.add(exp);
  }

  add(exp: any) {
    this.expservice.addExp(exp).then(
      () => {
        this.profileForm.reset();
        this.loading = false;
        location.reload();
      },
      (error) => {
        this.loading = false;
      }
    );
  }

}
