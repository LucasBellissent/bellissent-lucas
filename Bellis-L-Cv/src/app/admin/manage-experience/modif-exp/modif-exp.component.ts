import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ExpService } from 'src/app/services/exp.service';
import { SkillService } from 'src/app/services/skill.service';

@Component({
  selector: 'app-modif-exp',
  templateUrl: './modif-exp.component.html',
  styleUrls: ['./modif-exp.component.css']
})
export class ModifExpComponent implements OnInit {

  loading: boolean = false;

  exp: any = undefined;
  skills: any = undefined;

  id: string = '';

  newenv(tech: string): FormGroup {
    return this.fb.group({
      techno: tech,
    })
  }

  types = [
    'School Project',
    'Internship',
    'Pro'
  ]

  profileForm = this.fb.group({
    type: ['', [Validators.required]],
    entreprise: ['', [Validators.required, Validators.minLength(1)]],
    poste: ['', [Validators.required, Validators.minLength(1)]],
    description: ['', [Validators.required]],

    env: new FormArray([
    ]),
  });

  // dateForm = this.fb.group({
  //   start: ['', [Validators.required]],
  //   end: ['', [Validators.required]],
  // });

  range = new FormGroup({
    start: new FormControl(),
    end: new FormControl()
  });

  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private expservice: ExpService,
    private skillservice: SkillService,
  ) { }

  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];
    this.id = id;
    this.gets(id);
    this.getsskills();
  }


  gets(id: string) {
    this.expservice.getExp(id).then(
      (exp) => {
        // console.log(skills)
        this.exp = exp;
        this.completeform(exp);
      },
      (error) => {
        this.loading = false;
      }
    );
  }

  completeform(exp: any) {
    this.range.controls['start'].setValue(exp?.start);
    this.range.controls['end'].setValue(exp?.end);

    this.profileForm.controls['type'].setValue(exp?.type);
    this.profileForm.controls['entreprise'].setValue(exp?.entreprise);
    this.profileForm.controls['poste'].setValue(exp?.poste);
    this.profileForm.controls['description'].setValue(exp?.description);

    if (exp?.env != undefined)
      exp?.env.forEach((env: any) => {
        this.pushenv(env.techno)
      });
  }

  getsskills() {
    this.skillservice.getsName().then(
      (skills) => {
        // console.log(skills)
        this.skills = skills;
      },
      (error) => {
        this.loading = false;
      }
    );
  }

  get env(): FormArray {
    return this.profileForm.get("env") as FormArray
  }
  addenv() {
    this.env.push(this.newenv(''));
  }

  pushenv(env: string) {
    this.env.push(this.newenv(env));
  }

  deleteenv(index: number) {
    this.env.removeAt(index);
  }

  onSubmit() {
    this.loading = true;
    const exp: {} = {
      type: this.profileForm.get('type')!.value,

      start: this.range.value.start as Date,
      end: this.range.value.end as Date,

      entreprise: this.profileForm.get('entreprise')!.value,
      poste: this.profileForm.get('poste')!.value,
      description: this.profileForm.get('description')!.value,
      env: this.profileForm.get('env')!.value,
    };
    // console.log(exp)
    this.modif(exp);
  }

  modif(exp: any) {
    this.expservice.modifExp(this.id, exp).then(
      () => {
        this.profileForm.reset();
        this.loading = false;
        location.reload();
      },
      (error) => {
        this.loading = false;
      }
    );
  }
}
