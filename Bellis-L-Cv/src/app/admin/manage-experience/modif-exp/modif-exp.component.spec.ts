import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifExpComponent } from './modif-exp.component';

describe('ModifExpComponent', () => {
  let component: ModifExpComponent;
  let fixture: ComponentFixture<ModifExpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModifExpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifExpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
