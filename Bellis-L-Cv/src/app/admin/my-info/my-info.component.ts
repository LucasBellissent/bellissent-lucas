import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { User, UserInfo } from '../../services/models/user';
import { AlertService } from '../../services/alert.service';
import { UserService } from '../../services/user.service';
import { AuthenticationService } from '../../services/authentification.service';

@Component({
  selector: 'app-my-info',
  templateUrl: './my-info.component.html',
  styleUrls: ['./my-info.component.scss']
})
export class MyInfoComponent implements OnInit {

  currentUser: User | undefined;

  infoSubscription: Subscription | undefined;
  infouser?: UserInfo;

  prix: number = 0;

  validation: number = 0;
  preparation: number = 0;
  envoye: number = 0;
  remboursement: number = 0;

  status: string = '';
  // articles?: any;
  public imagePreview?: string;

  options = {
    autoClose: true,
    keepAfterRouteChange: false
  };

  constructor(
    protected alertService: AlertService,
    private userService: UserService,
    private authenticationService: AuthenticationService,
  ) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  ngOnInit(): void {
    this.getinfo();
  }

  getinfo() {
    if (this.currentUser?.roles == 'admin') {
      this.infoSubscription = this.userService.getinfo().subscribe(
        (value: any) => {
          // console.log(value)
          this.infouser = value;
        },
        (error) => {
          reject(error);
        });
    }

  }
}

function reject(error: any) {
  throw new Error('Function not implemented.');
}

