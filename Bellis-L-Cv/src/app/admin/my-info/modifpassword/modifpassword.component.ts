import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';
import { AuthenticationService } from 'src/app/services/authentification.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-modifpassword',
  templateUrl: './modifpassword.component.html',
  styleUrls: ['./modifpassword.component.scss']
})
export class ModifpasswordComponent implements OnInit {

  options = {
    autoClose: true,
    keepAfterRouteChange: false
};

profileForm = this.fb.group({
  // phone: this.phoneForm(),
  opassword: ['', [Validators.required, Validators.minLength(6), Validators.pattern('^[A-Za-z0-9]+$')]],
  password: ['', [Validators.required, Validators.minLength(6), Validators.pattern('^[A-Za-z0-9]+$')]],
  cpassword: ['', [Validators.required, Validators.minLength(6), Validators.pattern('^[A-Za-z0-9]+$')]],
  // email: ['', [Validators.pattern('^[A-Za-z0-9 @_.-]+$')]]
});

  constructor(
    protected alertService: AlertService,
    private fb: FormBuilder,
    private authenticationService: AuthenticationService,

    ) { }

  ngOnInit(): void {
  }

  changepass(old: string, newpw: string, confpw: string) {
    if (newpw == confpw) {
      this.authenticationService.modifpass(newpw, old);
    }
    else 
    this.alertService.error('les mots de passes ne sont pas identiques', this.options);
    }
}
