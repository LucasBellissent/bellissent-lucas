import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AppComponent } from 'src/app/app.component';
import { AlertService } from 'src/app/services/alert.service';
import { UserInfo } from 'src/app/services/models/user';
import { UserService } from 'src/app/services/user.service';
import { MyInfoComponent } from '../my-info.component';



@Component({
  selector: 'app-add-info',
  templateUrl: './add-info.component.html',
  styleUrls: ['./add-info.component.scss']
})

export class AddInfoComponent implements OnInit {

  public loading = false;

  PersonalForm = this.fb.group({
    civilite: [''],
    firstName: ['', [Validators.minLength(3), Validators.pattern('^[A-Za-zÀ-ÿ-]+$')]],
    lastName: ['', [Validators.minLength(3), Validators.pattern('^[A-Za-zÀ-ÿ-]+$')]],
    year: ['', [Validators.minLength(2), Validators.pattern('^[0-9]+$')]],

    phone: ['', [Validators.minLength(10), Validators.pattern('^[0-9+]+$')]],
    email : ['', [Validators.minLength(5), Validators.email]],

    ville: ['', [Validators.minLength(3), Validators.pattern('^[0-9A-Za-zÀ-ÿ- ]+$')]],
    pays: ['', [Validators.minLength(3), Validators.pattern('^[0-9A-Za-zÀ-ÿ- ]+$')]],

    school: ['', [Validators.minLength(3)]],
    work: ['', [Validators.minLength(3), Validators.pattern("^[0-9A-Za-zÀ-ÿ-'/'' ]+$")]],

    description: ['', [Validators.minLength(3)]],
  });

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    protected alertService: AlertService,
    ) { }

  ngOnInit(): void {
  }
  
  onSubmit() {
    const form: UserInfo = {
      civilite: this.PersonalForm.get('civilite')!.value,
      firstName: this.PersonalForm.get('firstName')!.value,
      lastName: this.PersonalForm.get('lastName')!.value,
      year: this.PersonalForm.get('year')!.value,

      phone: this.PersonalForm.get('phone')!.value,
      email: this.PersonalForm.get('email')!.value,

      pays: this.PersonalForm.get('pays')!.value,
      ville: this.PersonalForm.get('ville')!.value,

      school: this.PersonalForm.get('school')!.value,
      work: this.PersonalForm.get('work')!.value,

      description: this.PersonalForm.get('description')!.value,
    };
    console.log(form)
      this.userService.createInfo(form).then(
        () => {
          this.PersonalForm.reset();
          this.loading = false;
          location.reload();
        },
        (error) => {
          this.loading = false;
        }
      );
  }

}

