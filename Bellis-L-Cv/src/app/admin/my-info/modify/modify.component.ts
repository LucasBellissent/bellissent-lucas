import { ArrayType } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AlertService } from 'src/app/services/alert.service';
import { UserInfo } from 'src/app/services/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-modify',
  templateUrl: './modify.component.html',
  styleUrls: ['./modify.component.scss']
})
export class ModifyComponent implements OnInit {

  public loading = false;
  infosSubscription: Subscription | undefined;

  info: any;

  PersonalForm = this.fb.group({
    civilite: [''],
    firstName: ['', [Validators.minLength(3), Validators.pattern('^[A-Za-zÀ-ÿ-]+$')]],
    lastName: ['', [Validators.minLength(3), Validators.pattern('^[A-Za-zÀ-ÿ-]+$')]],
    year: ['', [Validators.minLength(2), Validators.pattern('^[0-9]+$')]],

    phone: ['', [Validators.minLength(10), Validators.pattern('^[0-9+]+$')]],
    email : ['', [Validators.minLength(5), Validators.email]],

    ville: ['', [Validators.minLength(3), Validators.pattern('^[0-9A-Za-zÀ-ÿ- ]+$')]],
    pays: ['', [Validators.minLength(3), Validators.pattern('^[0-9A-Za-zÀ-ÿ- ]+$')]],

    school: ['', [Validators.minLength(3)]],
    work: ['', [Validators.minLength(3), Validators.pattern("^[0-9A-Za-zÀ-ÿ-'/'' ]+$")]],

    description: ['', [Validators.minLength(3)]],
 });

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    protected alertService: AlertService,
  ) { }

  ngOnInit(): void {
    this.infosSubscription = this.userService.getinfo().subscribe(
      (info: any) => {
        this.info = info;
        this.completeInfo(info);
      }
    )
  }

  completeInfo(infos: any) {
    this.PersonalForm.controls['civilite'].setValue(infos?.civilite);
    this.PersonalForm.controls['firstName'].setValue(infos?.firstName);
    this.PersonalForm.controls['lastName'].setValue(infos?.lastName);
    this.PersonalForm.controls['year'].setValue(infos?.year);

    this.PersonalForm.controls['phone'].setValue(infos?.phone);
    this.PersonalForm.controls['email'].setValue(infos?.email);

    this.PersonalForm.controls['ville'].setValue(infos?.ville);
    this.PersonalForm.controls['pays'].setValue(infos?.pays);

    this.PersonalForm.controls['school'].setValue(infos?.school);
    this.PersonalForm.controls['work'].setValue(infos?.work);

    this.PersonalForm.controls['description'].setValue(infos?.description);
  }

  onSubmit() {
    this.loading = true;

    this.info.civilite = this.PersonalForm.get('civilite')!.value;
    this.info.firstName = this.PersonalForm.get('firstName')!.value;
    this.info.lastName = this.PersonalForm.get('lastName')!.value;
    this.info.year = this.PersonalForm.get('year')!.value;

    this.info.phone = this.PersonalForm.get('phone')!.value;
    this.info.email = this.PersonalForm.get('email')!.value;
    
    this.info.ville = this.PersonalForm.get('ville')!.value;
    this.info.pays = this.PersonalForm.get('pays')!.value;

    this.info.school = this.PersonalForm.get('school')!.value;
    this.info.work = this.PersonalForm.get('work')!.value;

    this.info.description = this.PersonalForm.get('description')!.value;

    // console.log(this.info);

    // console.log(this.info)
    this.userService.modifInfo(this.info)
    .then(
      () => {
        // this.articleForm.reset();
        this.loading = false;
        // this.router.navigate(['/articles']).then(() => {
        window.location.reload();
        // });
      },
      (error) => {
        this.loading = false;
      }
    );
  }
}
