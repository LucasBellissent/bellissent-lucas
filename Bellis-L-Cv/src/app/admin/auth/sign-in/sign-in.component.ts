import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { AlertService } from 'src/app/services/alert.service';
import { AuthenticationService } from 'src/app/services/authentification.service';
import { UserService } from 'src/app/services/user.service';
// import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
})
export class SignInComponent implements OnInit {
  errorMessage: string | undefined;
  returnUrl?: string;

   dataUser: Subscription | undefined;


  options = {
    autoClose: true,
    keepAfterRouteChange: false
  };

  profileForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(6), Validators.pattern('^[A-Za-z0-9]+$')]],
  });

  constructor(
    private router: Router,
    protected alertService: AlertService,
    // private auth: AuthService,
    private fb: FormBuilder,
    private authenticationService: AuthenticationService,
    protected usrservice: UserService,
    private route: ActivatedRoute,
  ) {
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  onLogin(emai: string, pswd: string) {
    // console.log(emai, ' st ', pswd);
    this.authenticationService.login(emai, pswd)
      .pipe(first())
      .subscribe(
        async data => {
          /* pb : impossible de récupere mes information avant l'appel 
                    de mon auth guard.
                    Si je l'appel trop tôt le token est pas généré et donc impossible d'obtenir mes information
                    Si non quand je l'appel aprés ma génération de token le temps que la requéte s'effectue sur le server
                    auth guard est déjà appeler
                */
          await sleep(100);
          this.router.navigate([this.returnUrl]).then(() => {
            window.location.reload();
          });
        },
        error => {
          // this.alertService.error(error, this.options);
          this.errorMessage = error;
        });
    //   this.auth
    //     .login(emai, pswd)
    //     .then(() => {
    //         this.router.navigate(['/part-three/all-stuff']);
    //     })
    //     .catch((error) => {
    //       this.errorMessage = error.message;
    //     });
    // }
  }
}

function sleep(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}