import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'src/app/services/alert.service';
// import { first } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/services/authentification.service';
// import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
})
export class SignUpComponent implements OnInit {
  signupForm?: FormGroup;
  errorMessage?: string;
  returnUrl?: string;
  status?: string;

  options = {
    autoClose: false,
    keepAfterRouteChange: false
  };

  options2 = {
    autoClose: true,
    keepAfterRouteChange: false
  };

  profileForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(6), Validators.pattern('^[A-Za-z0-9]+$')]],
    cpassword: ['', [Validators.required, Validators.minLength(6), Validators.pattern('^[A-Za-z0-9]+$')]],
  });

  constructor(
    // private router: Router,
    protected alertService: AlertService,
    // private auth: AuthService,
    private fb: FormBuilder,
    private authenticationService: AuthenticationService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.alertService.warn("le mots de passe doivent faire au minimum 6 caractéres", this.options);
  }

  chgstatus(value: any) {
    this.status = value;
    // console.log(value);

  }

  onSignup(emai: string, pswd: string, cfpswd: string) {
    // console.log("on y est")
    if (pswd == cfpswd)
        this.authenticationService.createNewUser(emai, pswd, this.returnUrl);

    else
      this.alertService.error("les mots de passe ne sont pas identiques", this.options2);

    // .pipe(first())
    // .subscribe(
    //     data => {
    //         this.router.navigate([this.returnUrl]);
    //     },
    //     error => {
    //         this.errorMessage = error;
    //         this.loading = false;
    //     });
    //   this.auth
    //     .createNewUser(emai, pswd)
    //     .then(() => {
    //       // this.router.navigate(['/part-three/all-stuff']);
    //       console.log("gg my boi")

    //     })
    //     .catch((error) => {
    //       this.loading = false;
    //       this.errorMessage = error.message;
    //     });
  }
}
