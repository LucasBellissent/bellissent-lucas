import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';
import { SkillService } from 'src/app/services/skill.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-manage-competences',
  templateUrl: './manage-competences.component.html',
  styleUrls: ['./manage-competences.component.css']
})
export class ManageCompetencesComponent implements OnInit {
  
  loading: boolean = false;

  skills: any =  undefined;

  searchText: string = '';
  type: string = 'nom';

  types = [
    'coding language',
    'framework',
    'database',

    'operating system',
    'code editor',
    'git',
    'container',

    'soft skills'
  ]

  profileForm = this.fb.group({
    nom: ['', [Validators.required, Validators.minLength(1)]],
    pourcent: ['', [Validators.required, Validators.minLength(1), Validators.pattern('^[0-9]+$')]],
    type: ['', [Validators.required]],
  });


  constructor(
    protected alertService: AlertService,
    private fb: FormBuilder,
    private skillservice: SkillService,
  ) { }

  ngOnInit(): void {
    this.gets();
  }

  onSubmit() {
    this.loading = true;
    const skill: {} = {
      nom: this.profileForm.get('nom')!.value,
      pourcent: this.profileForm.get('pourcent')!.value,
      type: this.profileForm.get('type')!.value,
    };
    // console.log(skill)
    this.add(skill);
  }

  gets() {
    this.skillservice.getsSkill().then(
      (skills) => {
        // console.log(skills)
        this.skills = skills;
      },
      (error) => {
        this.loading = false;
      }
    );
  }

  add(skill: any) {
    this.skillservice.addSkill(skill).then(
      () => {
        this.profileForm.reset();
        this.loading = false;
        location.reload();
      },
      (error) => {
        this.loading = false;
      }
    );
  }

  modif(id: string) {


    var inputValue = (<HTMLInputElement>document.getElementById('pourcent'+id)).value;
    // console.dir(inputValue)
   const updskill: {} =  {
      pourcent: inputValue,
    }
    // console.log(updskill, id)
    this.skillservice.modifSkill(updskill, id).then(
      () => {
        this.profileForm.reset();
        this.loading = false;
        location.reload();
      },
      (error) => {
        this.loading = false;
      }
    );
  }

  delete(skill: any) {
    console.log(skill)
    this.skillservice.deleteSkill(skill).then(
      () => {
        this.profileForm.reset();
        this.loading = false;
        location.reload();
      },
      (error) => {
        this.loading = false;
      }
    );
  }
  
  
  Changedtype(val: any) {
    // console.log(val.value);
    this.type = val.value;
  }
  
  Changed(val: any) {
    this.searchText = val.value;
  }
}

