import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageCompetencesComponent } from './manage-competences.component';

describe('ManageCompetencesComponent', () => {
  let component: ManageCompetencesComponent;
  let fixture: ComponentFixture<ManageCompetencesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageCompetencesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageCompetencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
