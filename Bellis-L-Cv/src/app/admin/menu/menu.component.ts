import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { User, UserInfo } from '../../services/models/user';
import { AuthenticationService } from '../../services/authentification.service';
// import { FormationService } from '../services/formation.service';
import { UserService } from '../../services/user.service';
// import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {
  // users?: User[];
  // myinfo?: string;
  // allUser?: any;
  dataUser: User | undefined;
  currentUser: any;

  public storageSub = new Subject<String>();
  // counterSubscription: Subscription | undefined;;
  // alluserSubscription: Subscription | undefined;;

  infoSubscription: Subscription | undefined;
  infouser?: UserInfo;


  constructor(
    private authservice: AuthenticationService,
    private router: Router,
    private userService: UserService,
    // private authenticationService: AuthenticationService,
  ) {
    this.currentUser = this.authservice.currentUserValue;
    // this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  ngOnInit() {

    this.infoSubscription = this.userService.getinfo().subscribe(
      (value: any) => {
        // console.log(value)
        this.infouser = value.info;
        // console.log(this.infouser);
      },
    );
    // console.log(this.currentUser?.roles);

    // this.myinfo = this.userService.getmy();
    // this.myinfo = this.userService.usr;
    // this.counterSubscription = this.userService.getmy().subscribe(
    //   (value: any) => {
    //     this.myinfo = value.user;
    //   });

    //   this.alluserSubscription = this.userService.getallinfo().subscribe(
    //     (value: any) => {
    //       this.allUser = value;
    //       console.log(this.allUser);
    //     })
    // this.userService.getAll().pipe(first()).subscribe(users => {
    //     this.users = users;
    // });
  }

  goto(route: any) {
    this.router.navigate([route]);
  }



  getinfo() {
    // this.myinfo = this.userservice.usr;
    // console.log(this.myinfo);
    // while (this.myinfo == undefined) {
    // this.myinfo = this.userService.getmy();
    // this.myinfo = this.userService.getmy();
    //   if (this.myinfo == undefined)
    // delay(6000000);
    // }
    // .subscribe(
    //   (value: any | undefined) => {
    //     this.myinfo = value;
    //   }
    // )
    // console.log(this.myinfo);
    // console.log(this.userservice.usr);
  }
}
