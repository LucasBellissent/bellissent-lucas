import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignInComponent } from './admin/auth/sign-in/sign-in.component';
import { SignUpComponent } from './admin/auth/sign-up/sign-up.component';
import { ManageCompetencesComponent } from './admin/manage-competences/manage-competences.component';
import { AddExpComponent } from './admin/manage-experience/add-exp/add-exp.component';
import { ManageExperienceComponent } from './admin/manage-experience/manage-experience.component';
import { ModifExpComponent } from './admin/manage-experience/modif-exp/modif-exp.component';
import { MenuComponent } from './admin/menu/menu.component';
import { ModifpasswordComponent } from './admin/my-info/modifpassword/modifpassword.component';
import { MyInfoComponent } from './admin/my-info/my-info.component';
import { ContactComponent } from './pages/contact/contact.component';
import { CursusComponent } from './pages/cursus/cursus.component';
import { ExperiencesComponent } from './pages/experiences/experiences.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { SkillsComponent } from './pages/skills/skills.component';
import { AuthGuard } from './services/auth-guard.service';

const routes: Routes = [
  //vue everyone
  { path: '', redirectTo: '/profile', pathMatch: 'full' },
  { path: 'profile', component: ProfileComponent },
  { path: 'cursus', component: CursusComponent },
  { path: 'skills', component: SkillsComponent },
  { path: 'experiences', component: ExperiencesComponent },
  { path: 'contact', component: ContactComponent },

  //vue admin
  { path: 'auth/login', component: SignInComponent },
  { path: 'auth/signup', component: SignUpComponent },  
  {
    path: 'modifpassword', component: ModifpasswordComponent,
    canActivate: [AuthGuard], 
    data: { roles: 'admin' }
  },
  {
    path: 'menu', component: MenuComponent,
    canActivate: [AuthGuard],
    data: { roles: 'admin' }
  },

  {
    path: 'manage-competences', component: ManageCompetencesComponent,
    canActivate: [AuthGuard],
    data: { roles: 'admin' }
  },
  
  {
    path: 'manage-experience', component: ManageExperienceComponent,
    canActivate: [AuthGuard],
    data: { roles: 'admin' }
  },
  {
    path: 'add-experience', component: AddExpComponent,
    canActivate: [AuthGuard],
    data: { roles: 'admin' }
  },
  {
    path: 'modif-experience/:id', component: ModifExpComponent,
    canActivate: [AuthGuard],
    data: { roles: 'admin' }
  },

  {
    path: 'info', component: MyInfoComponent,
    canActivate: [AuthGuard],
    data: { roles: 'admin' }
  },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    AuthGuard
  ]
})
export class AppRoutingModule { }
