import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { Subscription } from 'rxjs';
import { AlertService } from './alert.service';

import { AuthenticationService } from './authentification.service';
// import { UserService } from './user.service';

@Injectable()
export class AuthGuard implements CanActivate {

  infoSubscription: Subscription | undefined;
  userinfo: any;


  options = {
    autoClose: true,
    keepAfterRouteChange: false
  };

  constructor(
    private router: Router,
    protected alertService: AlertService,
    private authenticationService: AuthenticationService,
    // private usrservice: UserService
  ) {

  }
 
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const currentUser = this.authenticationService.currentUserValue;

    // console.log(currentUser)
    if (currentUser) {

      if (!route.data.roles || route.data.roles == currentUser?.roles
        // || (route.data.roles == 'user' && currentUser?.roles == 'admin')
      ) {
          return true;
      }
      else {
        this.router.navigate(['/profile']);
        this.alertService.error("vous n'avez pas les droits pour acceder a cette page", this.options);
        return false;
      }
    }
    this.router.navigate(['/profile'], { queryParams: { returnUrl: state.url } });
    this.alertService.error("Vous devez d'abbord vous authentifier", this.options);
    return false;
  }
}

function reject(error: any) {
  throw new Error('Function not implemented.');
}

