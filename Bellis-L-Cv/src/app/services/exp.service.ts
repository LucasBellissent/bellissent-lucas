import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { AlertService } from './alert.service';

@Injectable({
  providedIn: 'root'
})
export class ExpService {
  options = {
    autoClose: true,
    keepAfterRouteChange: false
  };

  constructor(
    private http: HttpClient,
    protected alertService: AlertService,
    private router: Router,
  ) { }

  addExp(exp: any) {
    return new Promise((resolve, reject) => {
      this.http.post<any>(
        `${environment.apiUrl}/exp/add`, exp)
        .subscribe(
          (response) => {
            resolve(response);
          },
          (error) => {
            this.alertService.error(error, this.options);
            reject(error);
          }
        )
    })
  }

  modifExp(id: string, exp: any) {
    return new Promise((resolve, reject) => {
      this.http.put<any>(
        `${environment.apiUrl}/exp/modif/` + id , exp)
        .subscribe(
          (response) => {
            resolve(response);
          },
          (error) => {
            this.alertService.error(error, this.options);
            reject(error);
          }
        )
    })
  }

  deleteExp(exp: any) {
    return new Promise((resolve, reject) => {
      this.http.delete<any>(
        `${environment.apiUrl}/exp/delete/`+ exp)
        .subscribe(
          (response) => {
            resolve(response);
          },
          (error) => {
            this.alertService.error(error, this.options);
            reject(error);
          }
        )
    })
  }

  getsExp() {
    return new Promise((resolve, reject) => {
      this.http.get<any>(
        `${environment.apiUrl}/exp/gets`)
        .subscribe(
          (response) => {
            resolve(response);
          },
          (error) => {
            this.alertService.error(error, this.options);
            reject(error);
          }
        )
    })
  }

  getExp(id: string) {
    return new Promise((resolve, reject) => {
      this.http.get<any>(
        `${environment.apiUrl}/exp/get/`+ id)
        .subscribe(
          (response) => {
            resolve(response);
          },
          (error) => {
            this.alertService.error(error, this.options);
            reject(error);
          }
        )
    })
  }
}
