import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { User, UserInfo } from './models/user';
import { BehaviorSubject, Observable, Subject, Subscription } from 'rxjs';

import { AlertService } from './alert.service';
import { Router } from '@angular/router';


@Injectable({ providedIn: 'root' })
export class UserService {
    id?: number;
    tst: any;
    destroyer!: Subscription;
    public user?: User;
    public usr: any;

    options = {
        autoClose: true,
        keepAfterRouteChange: false
    };

    private dataUserSubject: BehaviorSubject<UserInfo>;
    public dataUser: Observable<UserInfo>;

    constructor(
        protected alertService: AlertService,
        private router: Router,
        private http: HttpClient,
    ) {
        this.dataUserSubject = new BehaviorSubject<UserInfo>(JSON.parse(localStorage.getItem('currentinfo')!));
        this.dataUser = this.dataUserSubject.asObservable();
    }

    public get currentInfoValue(): UserInfo {
        return this.dataUserSubject.value;
    }

    createInfo(infos: any) {
        return new Promise((resolve, reject) => {
            this.http.post<any>(
                `${environment.apiUrl}/admin/createinfo`, infos)
                .subscribe(
                    async (msg) => {
                        this.gtinfo();
                        this.alertService.success(msg.message, this.options);
                        await sleep(100);
                        location.reload();
                    },
                    (error) => {
                        this.alertService.error(error, this.options);
                        reject(error);
                    }
                )
        })
    }

    modifInfo(infos: any) {
        return new Promise((resolve, reject) => {
            this.http.post<any>(
                `${environment.apiUrl}/admin/modifinfo`,
                { infos })
                .subscribe(
                    (response) => {
                        this.gtinfo();
                        resolve(response);
                    },
                    (error) => {
                        this.alertService.error(error, this.options);
                        reject(error);
                    }
                )
        })
    }

    getinfo() {
        return this.http.get<any>(
            `${environment.apiUrl}/admin/getinfo`,
            {})
    }

    gtinfo() {
        return this.http.get<any>(
            `${environment.apiUrl}/admin/getinfo`,
            {}).subscribe(
                (value: any) => {
                    localStorage.setItem('currentinfo', JSON.stringify(value));
                    this.dataUserSubject.next(value);
                },
                (error) => {
                    reject(error);
                })
    }

    gtprofile() {
        return this.http.get<any>(
            `${environment.apiUrl}/info/getinfo`,
            {})
    }

    contact(mail: any) {
        return new Promise((resolve, reject) => {
            this.http.post<any>(
                `${environment.apiUrl}/info/contact`,
                { mail })
                .subscribe(
                    (response) => {
                        // this.gtinfo();
                        resolve(response);
                    },
                    (error) => {
                        this.alertService.error(error, this.options);
                        reject(error);
                    }
                )
        })
    }

    destroyedinfo() {
        localStorage.removeItem('currentinfo');
        // this.dataUserSubject.next(new UserInfo());
        // this.dataUserSubject = new BehaviorSubject<UserInfo>(new UserInfo());
        this.dataUserSubject = new BehaviorSubject<any>([]);

    }

}

function reject(error: any) {
    throw new Error('Function not implemented.');
}
function sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

