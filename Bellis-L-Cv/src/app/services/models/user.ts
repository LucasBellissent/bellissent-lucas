export interface User {
  id: number;
  roles: string;
  token: string;
}

export interface UserInfo {
  email?: string;
  phone?: string;

  civilite?: string;
  firstName?: string;
  lastName?: string;
  year?: string;

  ville?: string;
  pays?: string;

  school?: string;
  work?: string;

  description?: string;

  // photoProfile?: string;
}

