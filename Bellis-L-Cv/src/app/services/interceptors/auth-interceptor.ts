import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AlertService } from '../alert.service';
// import { AuthService } from '../services/auth.service';
import { AuthenticationService } from '../authentification.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
 
  options = {
    autoClose: true,
    keepAfterRouteChange: false
};
 
  constructor(protected alertService: AlertService,
    // private auth: AuthService,
    private authenticationService: AuthenticationService) { }
  // intercept(req: HttpRequest<any>, next: HttpHandler) {
  //   const authToken = this.auth.token;
  //   const newRequest = req.clone({
  //     headers: req.headers.set('Authorization', 'Bearer ' + authToken),
  //   });
  //   return next.handle(newRequest);
  // }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(err => {
      if (err.status === 401) {
        // auto logout if 401 response returned from api
        // this.authenticationService.logout();
        // location.reload(true);
      }

      // const error = err.error.error || err.statusText;
      const error = err.error.message || err.statusText;
      // console.log("my err : ", error);
      this.alertService.error(error, this.options);
      return throwError(error);
    }))
  }
}
