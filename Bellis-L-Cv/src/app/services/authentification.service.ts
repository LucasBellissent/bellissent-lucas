import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { first, map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { User, UserInfo } from './models/user';
import { Router } from '@angular/router';
import { AlertService } from './alert.service';
import { UserService } from './user.service';


@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    errorMessage: any;

    options = {
        autoClose: true,
        keepAfterRouteChange: true
    };

    constructor(private http: HttpClient, private router: Router,
        protected usrservice: UserService,
        protected alertService: AlertService,
    ) {


        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')!));
        this.currentUser = this.currentUserSubject.asObservable();

    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    createNewUser(email: string, password: string,returnUrl?: string) {
        // console.log(status)
        return this.http.post<any>(
            `${environment.apiUrl}/auth/signup`,
            { email: email, password: password}).subscribe(
                (msg) => {
                    this.router.navigate(['/auth/login']);
                    this.alertService.success(msg.message, this.options);
                },
                (error) => {
                    reject(error);
                }
            )
    }

    login(email: string, password: string) {

        return this.http.post<any>(`${environment.apiUrl}/auth/login`, { email, password })
            .pipe(map(async user => {
                

                localStorage.setItem('currentUser', JSON.stringify(user));
                this.currentUserSubject.next(user);

                this.usrservice.gtinfo();
                
  
                return user;
            }));
    }

    forgetpassword(email: string) {
        {
            return this.http.post<any>(
                `${environment.apiUrl}/auth/resetpassword`,
                { email: email }).subscribe(
                    (msg) => {
                        this.router.navigate(['/auth/login']);
                        this.alertService.success(msg.message, this.options);
                    },
                    (error) => {
                        reject(error);
                    }
                )
        }
    }

    modifpass(newpw: string, oldpw: string) {
        return this.http.post<any>(
            `${environment.apiUrl}/auth/changepw`,
            { newpw: newpw, oldpw: oldpw })
            .subscribe(
                (msg) => {
                    this.alertService.success(msg.message, this.options);
                },
                (error) => {
                    reject(error);
                }
            )
    }

    // deletemyuser(password: string) {
    //     // console.log(id);
    //     return this.http.post<any>(
    //         `${environment.apiUrl}/info/delete`, { password })
    //         .subscribe(
    //             (msg) => {
    //                 this.logout();
    //                 // this.router.navigate(['/auth/login']);
    //                 this.alertService.success(msg.message, this.options);
    //             })
    // }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.usrservice.destroyedinfo();
        // this.currentUserSubject.next(new User());
        // this.currentUserSubject = new BehaviorSubject<User>(new User());
        this.currentUserSubject = new BehaviorSubject<any>([]);
        this.router.navigate(['/profile']).then(() => {
            window.location.reload();
        })
        // location.reload();
    }
}



function reject(error: any) {
    throw new Error(error);
}
