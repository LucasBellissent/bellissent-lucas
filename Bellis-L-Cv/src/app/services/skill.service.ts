import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { AlertService } from './alert.service';

@Injectable({
  providedIn: 'root'
})
export class SkillService {

  options = {
    autoClose: true,
    keepAfterRouteChange: false
  };

  constructor(
    private http: HttpClient,
    protected alertService: AlertService,
    private router: Router,
  ) { }

  addSkill(skill: any) {
    return new Promise((resolve, reject) => {
      this.http.post<any>(
        `${environment.apiUrl}/skill/add`, skill)
        .subscribe(
          (response) => {
            resolve(response);
          },
          (error) => {
            this.alertService.error(error, this.options);
            reject(error);
          }
        )
    })
  }

  modifSkill(skill: any, id: string) {
    return new Promise((resolve, reject) => {
      this.http.put<any>(
        `${environment.apiUrl}/skill/modif/` + id , skill)
        .subscribe(
          (response) => {
            resolve(response);
          },
          (error) => {
            this.alertService.error(error, this.options);
            reject(error);
          }
        )
    })
  }

  deleteSkill(skill: any) {
    return new Promise((resolve, reject) => {
      this.http.delete<any>(
        `${environment.apiUrl}/skill/delete/`+ skill)
        .subscribe(
          (response) => {
            resolve(response);
          },
          (error) => {
            this.alertService.error(error, this.options);
            reject(error);
          }
        )
    })
  }

  getsSkill() {
    return new Promise((resolve, reject) => {
      this.http.get<any>(
        `${environment.apiUrl}/skill/gets`)
        .subscribe(
          (response) => {
            resolve(response);
          },
          (error) => {
            this.alertService.error(error, this.options);
            reject(error);
          }
        )
    })
  }

  getsName() {
    return new Promise((resolve, reject) => {
      this.http.get<any>(
        `${environment.apiUrl}/skill/getsname`)
        .subscribe(
          (response) => {
            resolve(response);
          },
          (error) => {
            this.alertService.error(error, this.options);
            reject(error);
          }
        )
    })
  }

}
