import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

//rooting
import { AppRoutingModule } from './app-routing.module';
import { RoutesComponent } from './pages/routes/routes.component';

//framework front
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatTabsModule } from '@angular/material/tabs';
import { MatStepperModule } from '@angular/material/stepper';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';

import { NgPipesModule, NgArrayPipesModule } from 'ngx-pipes';

//components
import { AppComponent } from './app.component';
//not auth
import { PagesComponent } from './pages/pages.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { SkillsComponent } from './pages/skills/skills.component';
import { ExperiencesComponent } from './pages/experiences/experiences.component';
import { ContactComponent } from './pages/contact/contact.component';
import { CursusComponent } from './pages/cursus/cursus.component';
//admin
import { SignInComponent } from './admin/auth/sign-in/sign-in.component';
import { SignUpComponent } from './admin/auth/sign-up/sign-up.component';
import { ModifpasswordComponent } from './admin/my-info/modifpassword/modifpassword.component';
import { AddInfoComponent } from './admin/my-info/add-info/add-info.component';
import { MyInfoComponent } from './admin/my-info/my-info.component';
import { ModifyComponent } from './admin/my-info/modify/modify.component';
import { MenuComponent } from './admin/menu/menu.component';

import { ManageCompetencesComponent } from './admin/manage-competences/manage-competences.component';
import { ManageExperienceComponent } from './admin/manage-experience/manage-experience.component'
import { AddExpComponent } from './admin/manage-experience/add-exp/add-exp.component'
import { ModifExpComponent } from './admin/manage-experience/modif-exp/modif-exp.component'


import { AuthInterceptor } from './services/interceptors/auth-interceptor';
import { JwtInterceptor } from './services/interceptors/jwt.interceptor';

import { AlertComponent } from './services/alert/alert.component';



@NgModule({
  declarations: [
    AppComponent,
    PagesComponent,
    ProfileComponent,
    SkillsComponent,
    ExperiencesComponent,
    ContactComponent,
    RoutesComponent,
    CursusComponent,
    SignInComponent, 
    SignUpComponent,
    ModifpasswordComponent,
    MyInfoComponent, 
    ModifyComponent, 
    AddInfoComponent, 
    MenuComponent,
    AlertComponent,

    ManageCompetencesComponent,
    ManageExperienceComponent,
    AddExpComponent,
    ModifExpComponent,
  ],
  imports: [
    HttpClientModule,
    FormsModule, 
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    BrowserAnimationsModule,

    MatFormFieldModule,
    MatTabsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    MatStepperModule,
    MatRadioModule,
    MatSelectModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    MatInputModule,
    MatProgressBarModule,
    MatDatepickerModule,
    MatNativeDateModule,

    NgPipesModule,
    NgArrayPipesModule,
  ],
  providers: [
    // { provide: MatDialogRef, useValue: {} },
    // { provide: MAT_DIALOG_DATA, useValue: [] },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
