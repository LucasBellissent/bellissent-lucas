import { Component, OnInit } from '@angular/core';
import { SkillService } from 'src/app/services/skill.service';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements OnInit {
  //link skills to experiences
  //https://material.angular.io/components/progress-bar/api

  loading: boolean = false;
  skills: any =  undefined;
  type: string = 'pourcent';

  types = [
    'coding language',
    'framework',
    'database',

    'operating system',
    'code editor',
    'git',
    'container',

    'soft skills'
  ]

  constructor(
    private skillservice: SkillService,
  ) { }

  ngOnInit(): void {
    this.gets();
  }

  gets() {
    this.skillservice.getsSkill().then(
      (skills) => {
        // console.log(skills)
        this.skills = skills;
      },
      (error) => {
        this.loading = false;
      }
    );
  }

}
