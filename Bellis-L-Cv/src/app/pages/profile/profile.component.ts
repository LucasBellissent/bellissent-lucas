import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthenticationService } from 'src/app/services/authentification.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  currentuser : any;
  infoSubscription: Subscription | undefined;

  profile: any;

  constructor(private authenticationService: AuthenticationService,
    private userService: UserService,
    ) {
   this.currentuser = this.authenticationService.currentUserValue;
     }

  ngOnInit(): void {
    this.infoSubscription = this.userService.gtprofile().subscribe(
      (value: any) => {
        // console.log(value)
        this.profile = value;
      },
      (error) => {
        reject(error);
      });
  }

}

function reject(error: any) {
  throw new Error('Function not implemented.');
}