import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-routes',
  templateUrl: './routes.component.html',
  styleUrls: ['./routes.component.css']
})
export class RoutesComponent implements OnInit {
  rooter: any;

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.rooter = this.router;
  }

}
